# E-commerceApp - Angular frontend master

## Getting Started

- git clone (https://gitlab.com/martonszel/e-commerceapp)
- cd e-commerceapp/oshop/

## Prerequisites

Step 1 
- Angular-CLI is hosted as a node_module and to install and use it you’ll need NodeJS and it’s package manager named npm / Node Package Manager /

- To install NodeJS, navigate to the Downloads Section of the NodeJS’ website, download and install the Installer, best supported by your OS.

Step 2 
- `npm install -g @angular/cli`

Step 3
- `npm i ` - To install the dependencies 

Step 4 
- `ng serve --open` - The ng serve command should start a development server on your locahost port 4200, so if you go to your browser and enter the following url: http://localhost:4200 

## Project 


- Login with Google as Admin , Create/Edit/Delete product
![LoginOshop](img/LoginOshop.gif)

- Manage shoppingcart && check-out
![ManageCart](img/ManageCart.gif)


## Built With
- [Firebase](https://firebase.google.com/) - A comprehensive mobile development platform.
- [Bootstrap](https://getbootstrap.com/) - Bootstrap is an open source toolkit for developing with HTML, CSS, and JS.
- [NG-Bootstrap](https://ng-bootstrap.github.io/#/home/) - Angular widgets built from the ground up using only Bootstrap 4 CSS with APIs designed for the Angular ecosystem.

## Deployment

- [Oshop](https://oshop-15109.firebaseapp.com/)