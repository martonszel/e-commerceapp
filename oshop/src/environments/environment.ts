// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyAZ6qGMvyLj2jBQ7oLgNHuKOfelnTwqEuQ',
    authDomain: 'oshop-15109.firebaseapp.com',
    databaseURL: 'https://oshop-15109.firebaseio.com',
    projectId: 'oshop-15109',
    storageBucket: '',
    messagingSenderId: '453157085122',
    appId: '1:453157085122:web:b7a795c4067561f1'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
