import { Component, OnInit, OnDestroy, Input, ViewChildren, QueryList } from '@angular/core';
import { ProductService } from 'src/app/shared/services/product.service';
import { Observable, Subscription } from 'rxjs';
import { switchMap, map } from 'rxjs/operators';
import { Product } from 'src/app/shared/models/product';
import { NgbdSortableHeaderDirective, SortEvent } from 'src/app/sortable.directive';


@Component({
  selector: 'app-admin-products',
  templateUrl: './admin-products.component.html',
  styleUrls: ['./admin-products.component.css']
})
export class AdminProductsComponent implements OnDestroy  {

  @ViewChildren(NgbdSortableHeaderDirective) headers: QueryList<NgbdSortableHeaderDirective>;

  products: Product[] = [];
  filteredProducts: any [] = [];
  subscription: Subscription;

  page = 1;
  pageSize = 4;
  collectionSize = this.products.length;

  constructor(private productService: ProductService) {
     this.subscription = this.productService.getAll().subscribe(products => this.filteredProducts = this.products = products);


        }

  filter(query: string) {
    this.filteredProducts = (query) ?
    this.products.filter(p => p.title.toLowerCase().includes(query.toLowerCase())) :
    this.products;
  }



  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  compare(v1, v2) {
    return v1 < v2 ? -1 : v1 > v2 ? 1 : 0;
  }

  onSort({column, direction}: SortEvent) {

    // resetting other headers
    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = '';
      }
    });

    if (direction === '') {
      this.filteredProducts = this.products;
    } else {
      this.filteredProducts = [...this.products].sort((a, b) => {
        const res = this.compare(a[column], b[column]);
        return direction === 'asc' ? res : -res;
      });
    }
  }
  }

