import { ShoppingCartItem } from './shopping-cart-item';
import { Product } from './product';

export class ShoppingCart {
  items: ShoppingCartItem[] = [];

  constructor(private itemsMap: { [productId: string]: ShoppingCartItem }) {
    this.itemsMap = itemsMap || {};
    if (!itemsMap) { return; }
    for (const productId of Object.keys(itemsMap)) {

      if (itemsMap.hasOwnProperty(productId)) {
        const item = itemsMap[productId];
        this.items.push(new ShoppingCartItem({...item, key: productId}));
      }
    }
  }

  getQuantity(product: Product) {


    const item = this.itemsMap[product.key];
    return item ? item.quantity : 0 ;
  }

  get totalPrice() {
    let sum = 0;
    for (const productId in this.items) {
      if (this.items.hasOwnProperty(productId)) {
        sum += this.items[productId].totalPrice;
      }
    }
    return sum;
  }

  get productIds() {
    return Object.keys(this.items);
  }

  get totalItemsCount() {
    const keys = Object.entries(this.itemsMap);

    let count = 0;
    for (const productId of keys) {
      count += productId[1].quantity;
    }
    return count;
  }
}
