import { Component, OnInit } from '@angular/core';
import { AuthService } from 'shared/services/auth.service';
import { AppUser } from 'shared/models/app-user';
import { ShoppingCartService } from 'shared/services/shopping-cart.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ShoppingCart } from 'shared/models/shopping-cart';
import { AngularFireObject } from '@angular/fire/database';

@Component({
  selector: 'app-bs-navbar',
  templateUrl: './bs-navbar.component.html',
  styleUrls: ['./bs-navbar.component.css']
})
export class BsNavbarComponent implements OnInit {
  appUser: AppUser;
  cart$: Observable<ShoppingCart>;
  cart;
  shoppingCartItemCount: number;
  public isCollapsed = true;

  constructor(
    private auth: AuthService,
    private cartService: ShoppingCartService
  ) {}

  logout() {
    this.auth.logout();
  }

  async ngOnInit() {
    this.auth.appUser$.subscribe(appUser => (this.appUser = appUser));

    this.cart$ = await this.cartService.getCart();
    /*
    console.log(this.cart$)
    this.cart$.subscribe(cart=> {
      this.shoppingCartItemCount = 0 ;
      for (let productId in cart.items) {
        this.shoppingCartItemCount +=  cart.items[productId].quantity
      }})
    */
  }
}
